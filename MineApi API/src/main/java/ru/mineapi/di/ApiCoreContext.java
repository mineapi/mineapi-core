package ru.mineapi.di;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import ru.swayfarer.swl2.ioc.componentscan.DISwlSource;

/**
 * Отмеченный этой аннотацией класс станет одним из источников корневого контекста MineApi
 * @author swayfarer
 *
 */
@DISwlSource(context = MineApiContext.contextName)
@Retention(RUNTIME)
@Target(TYPE)
public @interface ApiCoreContext
{

}
