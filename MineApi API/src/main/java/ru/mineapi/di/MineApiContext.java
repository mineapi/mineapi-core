package ru.mineapi.di;

import ru.mineapi.config.MineApiConfig;
import ru.swayfarer.swl2.ioc.componentscan.ComponentEventType;
import ru.swayfarer.swl2.ioc.componentscan.DISwlComponentEvent;
import ru.swayfarer.swl2.logger.ILogger;
import ru.swayfarer.swl2.logger.LoggingManager;
import ru.swayfarer.swl2.markers.InternalElement;
import ru.swayfarer.swl2.resource.rlink.RLUtils;
import ru.swayfarer.swl2.resource.rlink.ResourceLink;
import ru.swayfarer.swl2.string.property.SystemProperty;

/**
 * Основной контекст MineApi <br>
 * @author swayfarer
 *
 */
@ApiCoreContext
public class MineApiContext {

	/** Логгер */
	@InternalElement
	public static ILogger logger = LoggingManager.getLogger();
	
	/** Имя контекста. Final потому, что иначе не впихнуть в аннотации */
	@InternalElement
	public static final String contextName = "mineapi|core";
	
	/** Проперти, в которой лежит расположение конфига MineApi */
	@ApiCore
	public SystemProperty mineApiConfigLocation()
	{
		return new SystemProperty("mineapi.config.location", MineApiConfig.defaultConfigLocation);
	}
	
	/** 
	 * Конфиг MineApi
	 * @param mineApiConfigLocation Проперти, через которую можно задать кастомное расположение конфига. Берется из контекста 
	 */
	@ApiCore
	public MineApiConfig mineApiCfg(SystemProperty mineApiConfigLocation)
	{
		ResourceLink configLoc = RLUtils.createLink(mineApiConfigLocation.getValue() + MineApiConfig.configName);
		
		if (!configLoc.isWritable())
		{
			logger.warning("Can't use api config from property", mineApiConfigLocation, "because it location is not writable! Using default location:", MineApiConfig.defaultConfigLocation);
			configLoc = RLUtils.createLink(MineApiConfig.defaultConfigLocation + MineApiConfig.configName);
		}
		
		return MineApiConfig.ofRLink(configLoc, MineApiConfig.class);
	}
	
	@DISwlComponentEvent(ComponentEventType.PreInit)
	public void initializeContext()
	{
		
	}
}
