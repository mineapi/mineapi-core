package ru.mineapi.di;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.swl2.ioc.componentscan.DISwlComponent;
import ru.swayfarer.swl2.ioc.context.elements.ContextElementType;

@DISwlComponent(context = MineApiContext.contextName)
@Retention(RUNTIME)
public @interface ApiCoreComponent
{
	/** Имя, под которым элемент будет создан */
	public String name() default "";
	
	/** Тип создаваемого элемента контекста */
	public ContextElementType type() default ContextElementType.Singleton;
	
	/** Класс, с которым элемент будет ассоциирован */
	public Class<?> associated() default Object.class;
}
