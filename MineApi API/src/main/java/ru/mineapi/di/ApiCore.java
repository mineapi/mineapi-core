package ru.mineapi.di;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

import ru.swayfarer.swl2.ioc.DIManager.DISwL;
import ru.swayfarer.swl2.ioc.context.elements.ContextElementType;

/**
 * Отмеченнный этой аннотацией
 * <h1> Поле: </h1>
 * 		Получит свое значение из корневого контекста MineApi <br>
 * <h1> Метод в объекте, зарегистрированном как источник контекста </h1> 
 * 		Станет источником элементов контекста MineApi
 * @author swayfarer
 *
 */
@DISwL(context = MineApiContext.contextName)
@Retention(RUNTIME)
public @interface ApiCore
{
	/** Имя элемента */
	public String name() default "";
	
	/** Использовать ли имя, или определять элемент только по типу? */
	public boolean usingName() default true;

	/** Тип элемента контекста (см {@link ContextElementType} ) */
	public ContextElementType type() default ContextElementType.Singleton;
}
