package ru.mineapi.di;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Отмеченные этой аннотацией элементы будут автоматически подхвачены MineApi и зарегистрированы. <br> 
 * Используется для, например, регистрации блоков и предметов без создания registry-класса <br> 
 * Для автоматической регистрации класс должен иметь конструктор без аргументов, иначе последует предупреждение в логи, а класс будет пропущен.
 * @author swayfarer
 *
 */
@Retention(RUNTIME)
public @interface AutoRegister
{
	/** Имя под которым будет зарегистрирован элемент*/
	public String name();
}
