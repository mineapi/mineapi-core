package ru.mineapi.mod;

import ru.mineapi.di.ApiCoreComponent;
import ru.mineapi.mod.loading.ModLoading;
import ru.mineapi.mod.loading.ModLoadingState;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.equals.EqualsUtils;
import ru.swayfarer.swl2.logger.ILogger;
import ru.swayfarer.swl2.logger.LoggingManager;
import ru.swayfarer.swl2.markers.InternalElement;

/**
 * Загрузчик модов из их сырых объектов <br> 
 * Чтение директории модов вынесено в отдельный модуль
 * @author swayfarer
 *
 */
@ApiCoreComponent(name = "modsLoader")
public class MineApiMods {

	/** Логгер */
	public static ILogger logger = LoggingManager.getLogger();
	
	/** Информация об уже загруженных модах */
	@InternalElement
	public IExtendedList<ModInfo> loadedMods = CollectionsSWL.createExtendedList();
	
	/**
	 * Загружен ли мод?
	 * @param modId Id мода
	 * @return Загружен ли?
	 */
	public boolean hasLoadedMod(String modId)
	{
		return getModById(modId) != null;
	}
	
	/**
	 * Получить мод из списка загруженных по его id
	 * @param modId Id мода
	 * @return Загруженный мод или null, если не найдется
	 */
	public ModInfo getModById(String modId)
	{
		for (ModInfo modInfo : loadedMods)
		{
			if (EqualsUtils.objectEqualsSome(modId, modInfo.getModId()));
				return modInfo;
		}
		
		return null;
	}
	
	/** Запуск инициализации загруженных модов */
 	public void setupLoadedMods()
	{
		preInitMods();
		initMods();
		postInitMods();
	}
 	
 	/**
 	 * Добавить мод для загрузки
 	 * @param modInfo Информация о загружаемом моде 
 	 */
 	public void loadMod(ModInfo modInfo)
 	{
 		loadedMods.addExclusive(modInfo);
 	}
	
	/** Пре-Инициализация модов */
	public void preInitMods()
	{
		logger.info("Pre-Initializing mods...");
		loadedMods.each((e) -> initMod(e, ModLoadingState.PreInit));
	}
	
	/** Инициализация модов */
	public void initMods()
	{
		logger.info("Initializing mods...");
		loadedMods.each((e) -> initMod(e, ModLoadingState.Init));
	}
	
	/** Пост-Инициализация модов */
	public void postInitMods()
	{
		logger.info("Post-Initializing mods...");
		loadedMods.each((e) -> initMod(e, ModLoadingState.PostInit));
	}
	
	/**
	 * Загрузить мод
	 * @param obj Экземпляр мода
	 */
	public void initMod(Object obj, ModLoadingState state)
	{
		invokeLoadingEvents(obj, state);
	}
	
	/**
	 * Вызвать методы, отмеченные аннотации {@link ModLoading}-аннотацией
	 * @param instance Объект, у которого вызываются методы
	 * @param state Этап загрузки мода
	 * @param args Аргументы, с которыми вызываются методы
	 */
	@InternalElement
	public void invokeLoadingEvents(Object instance, ModLoadingState state, Object... args)
	{
		ReflectionUtils.methods(instance)
			.annotated(ModLoading.class)
			.forArgs(args)
		.invoke(instance, args)
		;
	}
}
