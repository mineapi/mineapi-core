package ru.mineapi.mod;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.UUID;

import ru.swayfarer.swl2.version.SimpleVersion;

/**
 * Отмеченный этой аннотацией класс будет загружен сканнером модов на этапе загрузки MineApi
 * @author swayfarer 
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MineApiMod {
	
	/**
	 * Id мода. <br> 
	 * Строка, с которой MineApi будет идентифецировать мод. <br>
	 * В то время, как {@link #displayName()} может меняться, id должен оставаться неизменным, чтобы мод оставался собой <br>
	 * Хоть {@link UUID} юзай*) <br> <br>
	 * *не, не надо так =)
	 */
	public String id();
	
	/**
	 * Версия мода. По-умолчанию будет 1.0.0 <br>
	 * Версия мода должна соответствовать паттерну число.число.число, потому как будет прочитана как {@link SimpleVersion}
	 */
	public String version() default "1.0.0";
	
	/**
	 * Отображаемое имя мода. <br>
	 * Под этим именем мод будет фигурировать в менеджере модов
	 */
	public String displayName() default "";
	
}
