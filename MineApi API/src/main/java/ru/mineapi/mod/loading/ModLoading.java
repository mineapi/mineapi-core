package ru.mineapi.mod.loading;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Метод, выполняющийся на указанном этапе загрузки <br>
 * @author swayfarer
 *
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface ModLoading
{
	/** Этап, на котором загружается мод */
	public ModLoadingState value() default ModLoadingState.Init;
}
