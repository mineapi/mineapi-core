package ru.mineapi.mod.loading;

/**
 * Этапы загрузки мода
 * @author swayfarer
 *
 */
public enum ModLoadingState
{
	/** Пре-инициализация мода */
	PreInit,
	
	/** Инициализация */
	Init,
	
	/** Пост-инициализация мода*/
	PostInit
}
