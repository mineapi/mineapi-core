package ru.mineapi.mod;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import ru.swayfarer.swl2.resource.file.FileSWL;

/**
 * Информация о загруженном моде. <br> 
 * На этапе поиска модов {@link ModFinder} проверяет jar'ники и создает на их основе {@link ModInfo} <br> 
 * В дальнейшем MineApi будет использовать именно ModInfo 
 * 
 * @author swayfarer
 *
 */
@Data
@Accessors(chain = true)
@RequiredArgsConstructor
@AllArgsConstructor(staticName = "of")
public class ModInfo {

	/** Экземпляр мода (объект, созданный на основе класса мода) */
	public Object modInstance;
	
	/** Id мода */
	public String modId;
	
	/** Версия мода */
	public String modVersion;
	
	/** Файл, из которого был загружен мод */
	public FileSWL modSource;
	
}
