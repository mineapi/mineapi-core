package ru.mineapi.config;

import ru.swayfarer.swl2.markers.InternalElement;
import ru.swayfarer.swl2.observable.Observables;
import ru.swayfarer.swl2.observable.property.ObservableProperty;
import ru.swayfarer.swl2.swconf.config.AutoSerializableConfig;
import ru.swayfarer.swl2.swconf.serialization.comments.CommentSwconf;
import ru.swayfarer.swl2.swconf.serialization.comments.IgnoreSwconf;

/**
 * Конфигурация MineApi <br>
 * Содержит настройки запуска API, которые автоматически (де)сериаилизуюся в файл, 
 * находящийся {@link #defaultConfigLocation} 
 * или в системной проперти с именем {@link #configLocationPropertyName} <br>
 * Формат конфигурации автоматически подбирается по имени файла
 * @author swayfarer
 *
 */
public class MineApiConfig extends AutoSerializableConfig {

	/** Стандартное расположение файла конфигурации */
	@InternalElement
	@IgnoreSwconf
	public static String defaultConfigLocation = "f:config/mineapi/";
	
	@InternalElement
	@IgnoreSwconf
	public static String configName = "conf.lua";
	
	/** Имя системной проперти, через которую можно изменить значение {@link #defaultConfigLocation} */
	@InternalElement
	@IgnoreSwconf
	public static String configLocationPropertyName = "";
	
	/** 
	 * Запущен ли MineApi на клиентской стороне? <br>
	 * На клиентской стороне доступен клиентский API, вроде рендера и т.п., который не доступен на сервере. <br>
	 * Чтобы не получить {@link ClassNotFoundException} и т.п., т.е. не обратиться к несуществующему в класпасе API, можно переключать клиент/сервер при помощи этой проперти
	 */
	@CommentSwconf("Is there client side?")
	public ObservableProperty<Boolean> isClient = Observables.createProperty(true);
	
	/** Директория модов */
	@CommentSwconf("The directory where mods located")
	public ObservableProperty<String> modsDir = Observables.createProperty("mineapimods");
}
