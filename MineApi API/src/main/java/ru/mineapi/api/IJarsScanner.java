package ru.mineapi.api;

import ru.mineapi.di.AutoRegister;
import ru.mineapi.mod.MineApiMod;
import ru.swayfarer.swl2.collections.extended.IExtendedList;

/**
 * Интерфейс для взаимодействия со сканированием файлов в папке модов. <br> 
 * Реализуется отдельным модулем и доступен через DI
 * @author swayfarer
 *
 */
public interface IJarsScanner {

	/** Найти все классы, отмеченные аннотацией {@link MineApiMod} */
	public IExtendedList<Class<?>> findModAnnotatedClasses();
	
	/** Найти все классы, отмеченные аннотацией {@link AutoRegister} */
	public IExtendedList<Class<?>> findAutoRegistryClasses();
	
}
