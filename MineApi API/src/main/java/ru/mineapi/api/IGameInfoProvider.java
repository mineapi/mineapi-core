package ru.mineapi.api;

/**
 * Интерфейс для получения информации об игре <br>
 * Позволяет узнать информацию об игре, такую как: представлен ли клиентский API, версию игры и пр. <br>
 * Реализуется во внутреннем модуле.
 * @author swayfarer
 *
 */
public interface IGameInfoProvider {

	/** Доступен ли клиентский API в игре? */
	public boolean isClientSideProvided();
	
}
