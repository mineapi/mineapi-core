package ru.mineapi.proxy;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Отмеченные этой аннотацией элементы будут видимы только на клиенте <br>
 * На серверной стороне их не будет видно.
 * @author swayfarer
 *
 */
@Retention(RUNTIME)
public @interface ClientOnly {

}
