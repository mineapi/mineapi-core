package ru.mineapi.proxy;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/**
 * Отмеченные этой аннотацией элементы будут видимы только на сервере <br>
 * На клиентской стороне их не будет видно.
 * @author swayfarer
 *
 */
@Retention(RUNTIME)
public @interface ServerOnly {

}
