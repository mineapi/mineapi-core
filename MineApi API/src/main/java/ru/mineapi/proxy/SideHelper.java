package ru.mineapi.proxy;

import ru.mineapi.api.IGameInfoProvider;
import ru.mineapi.config.MineApiConfig;
import ru.mineapi.di.ApiCore;
import ru.mineapi.di.ApiCoreComponent;
import ru.swayfarer.swl2.markers.InternalElement;

/**
 * Помощник для работы с клиентской и серверной стороной <br>
 * Позволяет определить сторону (клиент/сервер) на основании настроек MineApi и возможностей клиента
 * @author swayfarer
 *
 */
@ApiCoreComponent
public class SideHelper {

	/** Конфигурация MineApi */
	@InternalElement
	@ApiCore
	public MineApiConfig mineApiCfg;
	
	/** Провайдер информации об игре */
	@InternalElement
	@ApiCore
	public IGameInfoProvider mcInfoProvider;
	
	/** Находимся ли мы на серверной стороне? */
	public boolean isServerSide()
	{
		return !isClientSide();
	}
	
	/** Находимся ли мы на клиентской стороне? */
	public boolean isClientSide()
	{
		return mineApiCfg.isClient.get() && mcInfoProvider.isClientSideProvided();
	}
}
